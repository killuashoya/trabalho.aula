﻿using System;
using System.Collections.Generic;

namespace trabalho.Diogo.ZeSousa
{
    class Program
    {
        private static int length;

        static void Main(string[] args)
        {
            Console.WriteLine("◄ Bem Vindo ►");
            Console.WriteLine("Oque gostaria de pesquisar?");
            Console.WriteLine("(hoodie, t-shirt,calças.)");

            //________________________________________________________________

            #region hoodies
            List<hoodie> hoodies = new List<hoodie>();
            //_______________________
            hoodie carhartt = new hoodie("carhartt", "M", 80);
            hoodie lacoste = new hoodie("lacoste", "S", 70);
            hoodie tommy = new hoodie("tommy", "L", 50);
            hoodie champion = new hoodie("champion", "XL", 40);
            hoodie ralph = new hoodie("ralph", "XS", 90);
            #endregion

            //________________________________________________________________

            #region t-shirt
            List<t_shirt> tshirts = new List<t_shirt>();
            //_______________________
            t_shirt NY = new t_shirt("MLB New York Yankees", "L", 35);
            t_shirt Champion = new t_shirt("champion basica", "M", 20);
            t_shirt Nike = new t_shirt("Nike basica", "XL", 50);
            t_shirt adidas = new t_shirt("Adidas desportiva", "M", 29);
            t_shirt calvin  = new t_shirt("calvin klein basica", "XS", 90);
            #endregion

            //________________________________________________________________

            #region calças
            List<calça> calcas = new List<calça>();
            //_______________________
            calça tiffosi = new calça("ganga", "TIFFOSI basica", 48, 35);
            calça Carhartt = new calça("ganga", "Carhartt WIP Simple", 44, 50);
            calça springfield= new calça("ganga", "springfield basica", 48, 35);
            calça Carhartt_J = new calça("cargo", "Carhartt Regular Cargo", 48, 40);
            calça Lacoste = new calça("jogging", "calças jogging", 46, 100);
            #endregion

            //________________________________________________________________
            #region cash
            
            #endregion
            //________________________________________________________________
            #region hoodiesresp
            string resp1 = Console.ReadLine();
            if (resp1 == "hoodie")
            {
                Console.WriteLine("Em loja temos " + carhartt.nome1 + ", " + lacoste.nome1 + ", " + tommy.nome1 + ", " + champion.nome1 + ", " + ralph.nome1);

                Console.WriteLine("Gostaria de ver algum modelo em específico?");
                string resphoodie = Console.ReadLine();
                if (resphoodie == "carhartt")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + carhartt.tamanho1 + " e a custar " + carhartt.preco1);
                }
                if (resphoodie == "lacoste")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + lacoste.tamanho1 + " e a custar " + lacoste.preco1);
                }
                if (resphoodie == "tommy")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + tommy.tamanho1 + " e a custar " + tommy.preco1);
                }
                if (resphoodie == "champion")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + champion.tamanho1 + " e a custar " + champion.preco1);
                }
                if (resphoodie == "ralph")
                { 
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + ralph.tamanho1 + " e a custar " + ralph.preco1);
                }
            }
            #endregion 
            //________________________________________________________________
            #region tshirt
            if (resp1 == "t-shirt")
            {
                Console.WriteLine("Em loja temos " + NY.nome2 + ", " + Champion.nome2 + ", " + Nike.nome2 + ", " + adidas.nome2 + ", " + calvin.nome2);

                Console.WriteLine("Gostaria de ver algum modelo em específico?");
                string resptshirt = Console.ReadLine();

                if (resptshirt == "NY")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + NY.tamanho2 + " e a custar " + NY.preco2);
                }
                if (resptshirt == "Champion")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Champion.tamanho2 + " e a custar " + Champion.preco2);
                }
                if (resptshirt == "Nike")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Nike.tamanho2 + " e a custar " + Nike.preco2);
                }
                if (resptshirt == "adidas")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + adidas.tamanho2 + " e a custar " + adidas.preco2);
                }
                if (resptshirt == "calvin")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + calvin.tamanho2 + " e a custar " + calvin.preco2);
                }
            }
            #endregion
            //________________________________________________________________
            #region calcas 
            if (resp1 == "calça")
            {
                Console.WriteLine("Em loja temos " + tiffosi.nome3 + ", " + Carhartt.nome3 + ", " + springfield.nome3 + ", " + Carhartt_J.nome3 + ", " + Lacoste.nome3);

                Console.WriteLine("Gostaria de ver algum modelo em específico?");
                string respcalcas = Console.ReadLine();

                if (respcalcas == "tiffosi")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + tiffosi.tamanho3 + " e a custar " + tiffosi.preco3);
                }
                if (respcalcas == "Carhartt")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Carhartt.tamanho3 + " e a custar " + Carhartt.preco3);
                }
                if (respcalcas == "springfield")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + springfield.tamanho3 + " e a custar " + springfield.preco3);
                }
                if (respcalcas == "Carhartt_J")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Carhartt_J.tamanho3 + " e a custar " + Carhartt_J.preco3);
                }
                if (respcalcas == "Lacoste")
                {
                    Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Lacoste.tamanho3 + " e a custar " + Lacoste.preco3);
                }

            }
            #endregion
            //________________________________________________________________


             
        }
    }
}
